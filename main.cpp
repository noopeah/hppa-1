#include <iostream>
#include <iomanip>
#include <xmmintrin.h>
#include <immintrin.h>
#include <random>
#include <chrono>
#include <iterator>
#include <functional>

#define SUP_SZ  256
#define SUB_SZ  8

/*
for parameters:
    SUP_SZ = 128, SUB_SZ = 8 (total mtx size = 1024)

        unvectorized    timed execution <uS>:    591774
          vectorized    timed execution <uS>:    375833 ~1.5x   faster than unvec
 manually vectorized    timed execution <uS>:    195052 ~3x     faster than unvec
1 1

for parameters:
    SUP_SZ = 512, SUB_SZ = 8 (total mtx size = 4096)
        unvectorized    timed execution <uS>:  37010199
          vectorized    timed execution <uS>:  24671632 ~1.5x   faster than unvec
 manually vectorized    timed execution <uS>:  12532784 ~3x     faster than unvec
1 1
*/

double** newMatrix()
{
    auto* matrix = new double* [SUB_SZ];
    for (int i = 0; i < SUB_SZ; i++)
    {
        matrix[i] = new double[SUB_SZ];
    }
    return matrix;
}

double getRandomDouble()
{
    return static_cast<double>(rand()) / static_cast <double> (RAND_MAX);
}

bool eq(
	 double** a
	,double** b
){
	for (int i = 0; i < SUB_SZ; i++)
	{
		for (int j = 0; j < SUB_SZ; j++)
		{
			if(a[i][j] != b[i][j]){
				std::cout <<
					"i=" << i << " " <<
					"j=" << j << " " <<
					"a=" << a[i][j] << " " <<
					"b=" << b[i][j] << " " << std::endl;
				return false;
			}
		}
	}
	return true;
}

bool superEq(
     double**** a
    ,double**** b
){
    for (int i = 0; i < SUP_SZ; i++)
	{
		for (int j = 0; j < SUP_SZ; j++)
		{
			if(!eq(a[i][j], b[i][j]))
            {
				std::cout <<std::endl <<
					"SUPER i=" << i << " " <<
					"SUPER j=" << j << std::endl;
				return false;
			}
		}
	}
	return true;
}

void initMatrix(
	double** matrix
){
    for (int i = 0; i < SUB_SZ; i++)
    {
        for (int j = 0; j < SUB_SZ; j++)
        {
        	matrix[i][j] = getRandomDouble();
        }
    }
}

double**** newSuperMatrix(
    bool initWithRandoms = false
){
    auto* matrix = new double*** [SUP_SZ];
    for (int i = 0; i < SUP_SZ; i++)
    {
        matrix[i] = new double**[SUP_SZ];
    }

    for (int i = 0; i < SUP_SZ; i++)
    {
        for (int j = 0; j < SUP_SZ; j++)
        {
        	matrix[i][j] = newMatrix();
        }
    }
    return matrix;
    if (!initWithRandoms) return matrix;

    for (int i = 0; i < SUP_SZ; i++)
    {
        for (int j = 0; j < SUP_SZ; j++)
        {
        	initMatrix(matrix[i][j]);
        }
    }
    return matrix;
}

void _plainMul(
    double** mtxA
	,double** mtxB
	,double** product
){
    double* aCol;
    double* pRow;
    double* bCol;

	for (int i = 0; i < SUB_SZ; i++)
	{
		aCol = mtxA[i];
		pRow = product[i];

		for (int j = 0; j < SUB_SZ; j++)
		{
			bCol = mtxB[j];

			#pragma clang loop vectorize(disable) interleave(disable)
			for (int k = 0; k < SUB_SZ; k++)
			{
				pRow[k] += bCol[j] * aCol[k];
			}
		}
	}
}

void _plainAdd(
	double** mtxA
	,double** mtxB
	,double** sum 
){
    double* aCol;
    double* pRow;
    double* bCol;

	for (int i = 0; i < SUB_SZ; i++)
	{
		aCol = mtxA[i];
        bCol = mtxB[i];
		pRow = sum[i];

        #pragma clang loop vectorize(disable) interleave(disable) unroll(disable)
		for (int j = 0; j < SUB_SZ; j++)
		{
            pRow[j] = bCol[j] + aCol[j];
		}
	}
}

void _autovecMul(
	double** mtxA
	,double** mtxB
	,double** product
	,int superSecretMegaDynamicValueIDontWantThisLoopToUnrollInto8LoadsAndAddsAndMuls
){
    double* aCol;
    double* pRow;
    double* bCol;

	for (int i = 0; i < SUB_SZ; i++)
	{
		aCol = mtxA[i];
		pRow = product[i];

		for (int j = 0; j < SUB_SZ; j++)
		{
			bCol = mtxB[j];

			#pragma clang loop vectorize(enable) interleave(enable) unroll(disable)
			for (int k = 0; k < SUB_SZ; k++)
			{
				pRow[k] += bCol[j] * aCol[k];
			}
		}
	}
}

void _autovecAdd(
	double** mtxA
	,double** mtxB
	,double** product
){
    double* aCol;
    double* pRow;
    double* bCol;

	for (int i = 0; i < SUB_SZ; i++)
	{
		aCol = mtxA[i];
        bCol = mtxB[i];
		pRow = product[i];

        #pragma clang loop vectorize(enable) interleave(enable)
		for (int j = 0; j < SUB_SZ; j++)
		{
            pRow[j] = bCol[j] + aCol[j];
		}
	}
}

void _manualMul(
	double** mtxA
	,double** mtxB
	,double** product
){
	for (int i = 0; i < SUB_SZ; ++i)
	{
		for (int j = 0; j < SUB_SZ; j += 4)
		{
			__m256 sum = _mm256_loadu_pd(product[i] + j);
			for (int k = 0; k < SUB_SZ; ++k)
			{
				sum = _mm256_fmadd_pd(_mm256_set1_pd(mtxA[i][k]), _mm256_loadu_pd(mtxB[k] + j), sum);
			}
			_mm256_storeu_pd(product[i] + j, sum);
		}
	}
}

double**** plainMul(
    double**** mtxA
	,double**** mtxB
){
    double**** product = newSuperMatrix();
    double** temp = newMatrix();
    double*** aCol;
    double*** pRow;
    double*** bCol;

    for (int superI = 0; superI < SUP_SZ; superI++)
    {
        aCol = mtxA[superI];
		pRow = product[superI];
        for (int superJ = 0; superJ < SUP_SZ; superJ++)
        {
            bCol = mtxB[superJ];
            #pragma clang loop vectorize(disable) interleave(disable)
            for (int superK = 0; superK < SUP_SZ; superK++)
            {
            	_plainMul(bCol[superJ], aCol[superK], temp);
                _plainAdd(temp, pRow[superK], pRow[superK]);
            }
        }
    }
	return product;
}

double**** autovecMul(
    double**** mtxA
	,double**** mtxB
){
    double**** product = newSuperMatrix();
    double** temp = newMatrix();
    double*** aCol;
    double*** pRow;
    double*** bCol;

    for (int superI = 0; superI < SUP_SZ; superI++)
    {
        aCol = mtxA[superI];
		pRow = product[superI];
        for (int superJ = 0; superJ < SUP_SZ; superJ++)
        {
            bCol = mtxB[superJ];
            #pragma clang loop vectorize(enable) interleave(enable)
            for (int superK = 0; superK < SUP_SZ; superK++)
            {
            	_autovecMul(bCol[superJ], aCol[superK], temp, SUB_SZ);
                _autovecAdd(temp, pRow[superK], pRow[superK]);         
			}
        }
    }
	return product;
}

double**** manualMul(
    double**** mtxA
	,double**** mtxB
){
    double**** product = newSuperMatrix();

    double*** aCol;
    double*** pRow;
    double*** bCol;
    double** temp = newMatrix();
    for (int superI = 0; superI < SUP_SZ; superI++)
    {
        aCol = mtxA[superI];
		pRow = product[superI];
        for (int superJ = 0; superJ < SUP_SZ; superJ++)
        {
            bCol = mtxB[superJ];
            #pragma clang loop vectorize(enable) interleave(enable)
            for (int superK = 0; superK < SUP_SZ; superK++)
            {
            	_manualMul(bCol[superJ], aCol[superK], temp);
                _autovecAdd(temp, pRow[superK], pRow[superK]);    
            }
        }
    }
	return product;
}

double**** executeTimed(
	double****(caller)(double****, double****)
	,double**** a
	,double**** b
	,const char* message) 
{
	auto timeStart = std::chrono::high_resolution_clock::now();
	auto* result = caller(a, b);
    auto timeEnd = std::chrono::high_resolution_clock::now();
    std::cout
    	<< std::setw(20)
    	<< message
    	<< std::setw(25)
    	<< "timed execution <uS>:"
    	<< std::setw(10)
    	<< std::chrono::duration_cast<std::chrono::microseconds>(timeEnd - timeStart).count() 
    	<< std::endl;
    return result;
}

int main()
{
    srand(static_cast <unsigned> (2281337));
	double**** a = newSuperMatrix(true);
	double**** b = newSuperMatrix(true);

	auto* plainRes = executeTimed(plainMul, a, b, "unvectorized");
	auto* vecRes = executeTimed(autovecMul, a, b, "vectorized");
	auto* manVecRes = executeTimed(manualMul, a, b, "manually vectorized");

	std::cout << (superEq(plainRes, vecRes)) << " " << (superEq(vecRes, manVecRes)) << std::endl;
	
	delete a;
	delete b;

	delete vecRes;
	delete plainRes;
	delete manVecRes;
	return 0;
}

